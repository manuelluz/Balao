﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Balao
{
    public partial class Form1 : Form
    {

        private Balao MeuBalao;

        public Form1()
        {
            InitializeComponent();

            MeuBalao = new Balao();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //ComboBoxCor.Items.Add("Amarelo");
            //ComboBoxCor.Items.Add("Vermelho");
            //ComboBoxCor.Items.Add("Azul");
            ComboBoxCor.Items.AddRange(new string[] { "Vermelho", "Azul", "Amarelo" });


            LabelAltura.Text = "0";
            //LabelMessagem.Text = MeuBalao.ToString();
            
        }    

        private void ButtonAumentar_Click(object sender, EventArgs e)
        {
            MeuBalao.Altura++;
            LabelAltura.Text = MeuBalao.Altura.ToString();

        }

        private void ButtonDiminuir_Click(object sender, EventArgs e)
        {
            MeuBalao.Altura--;
            LabelAltura.Text = MeuBalao.Altura.ToString();
            
        }

        private void LabelMessagem_Click(object sender, EventArgs e)
        {
            //MeuBalao.Messagem.ToString();
            //LabelMessagem.Text = ToString();
        }
    }
}
