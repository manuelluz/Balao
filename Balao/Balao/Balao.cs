﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balao
{
    class Balao
    {

        #region Atributos

        private int _cor;

        private int _altura;

        private string _messagem;

        #endregion

        #region propriedades

        public int Altura
        {
            get { return _altura; }

            set
            {
                if (value >= 0)
                {
                    _altura = value;
                }
            }
        }

        public string Cor { get; set; }

        public string Mensagem { get; private set; }

        //public Balao(string cor, int altura, string messagem)
        //{
        //    Cor = cor;
        //    Altura = altura;
        //    Mensagem = messagem;
        //}

        #endregion



        #region Construtores


        #endregion

        public override string ToString()
        {
            return String.Format("O balao {0}, Atingiu {1} metros de altura", Cor, Altura);
        }
    }
}
