﻿namespace Balao
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelBalao = new System.Windows.Forms.Label();
            this.ComboBoxCor = new System.Windows.Forms.ComboBox();
            this.LabelCor = new System.Windows.Forms.Label();
            this.LabelAltura = new System.Windows.Forms.Label();
            this.ButtonAumentar = new System.Windows.Forms.Button();
            this.ButtonDiminuir = new System.Windows.Forms.Button();
            this.LabelMessagem = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LabelBalao
            // 
            this.LabelBalao.AutoSize = true;
            this.LabelBalao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelBalao.Location = new System.Drawing.Point(85, 21);
            this.LabelBalao.Name = "LabelBalao";
            this.LabelBalao.Size = new System.Drawing.Size(55, 20);
            this.LabelBalao.TabIndex = 0;
            this.LabelBalao.Text = "Balão";
            // 
            // ComboBoxCor
            // 
            this.ComboBoxCor.FormattingEnabled = true;
            this.ComboBoxCor.Location = new System.Drawing.Point(117, 83);
            this.ComboBoxCor.Name = "ComboBoxCor";
            this.ComboBoxCor.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxCor.TabIndex = 1;
            this.ComboBoxCor.Text = "Selecione";
            // 
            // LabelCor
            // 
            this.LabelCor.AutoSize = true;
            this.LabelCor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelCor.Location = new System.Drawing.Point(13, 83);
            this.LabelCor.Name = "LabelCor";
            this.LabelCor.Size = new System.Drawing.Size(98, 16);
            this.LabelCor.TabIndex = 2;
            this.LabelCor.Text = "Cor do balão";
            // 
            // LabelAltura
            // 
            this.LabelAltura.AutoSize = true;
            this.LabelAltura.Location = new System.Drawing.Point(106, 128);
            this.LabelAltura.Name = "LabelAltura";
            this.LabelAltura.Size = new System.Drawing.Size(34, 13);
            this.LabelAltura.TabIndex = 3;
            this.LabelAltura.Text = "Altura";
            // 
            // ButtonAumentar
            // 
            this.ButtonAumentar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAumentar.Location = new System.Drawing.Point(16, 123);
            this.ButtonAumentar.Name = "ButtonAumentar";
            this.ButtonAumentar.Size = new System.Drawing.Size(75, 23);
            this.ButtonAumentar.TabIndex = 4;
            this.ButtonAumentar.Text = "+";
            this.ButtonAumentar.UseVisualStyleBackColor = true;
            this.ButtonAumentar.Click += new System.EventHandler(this.ButtonAumentar_Click);
            // 
            // ButtonDiminuir
            // 
            this.ButtonDiminuir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonDiminuir.Location = new System.Drawing.Point(153, 123);
            this.ButtonDiminuir.Name = "ButtonDiminuir";
            this.ButtonDiminuir.Size = new System.Drawing.Size(75, 23);
            this.ButtonDiminuir.TabIndex = 5;
            this.ButtonDiminuir.Text = "-";
            this.ButtonDiminuir.UseVisualStyleBackColor = true;
            this.ButtonDiminuir.Click += new System.EventHandler(this.ButtonDiminuir_Click);
            // 
            // LabelMessagem
            // 
            this.LabelMessagem.AutoSize = true;
            this.LabelMessagem.Location = new System.Drawing.Point(16, 184);
            this.LabelMessagem.Name = "LabelMessagem";
            this.LabelMessagem.Size = new System.Drawing.Size(58, 13);
            this.LabelMessagem.TabIndex = 6;
            this.LabelMessagem.Text = "Messagem";
            this.LabelMessagem.Click += new System.EventHandler(this.LabelMessagem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 253);
            this.Controls.Add(this.LabelMessagem);
            this.Controls.Add(this.ButtonDiminuir);
            this.Controls.Add(this.ButtonAumentar);
            this.Controls.Add(this.LabelAltura);
            this.Controls.Add(this.LabelCor);
            this.Controls.Add(this.ComboBoxCor);
            this.Controls.Add(this.LabelBalao);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelBalao;
        private System.Windows.Forms.ComboBox ComboBoxCor;
        private System.Windows.Forms.Label LabelCor;
        private System.Windows.Forms.Label LabelAltura;
        private System.Windows.Forms.Button ButtonAumentar;
        private System.Windows.Forms.Button ButtonDiminuir;
        private System.Windows.Forms.Label LabelMessagem;
    }
}

